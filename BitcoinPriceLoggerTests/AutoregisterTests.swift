//
//  AutoregisterTests.swift
//  BitcoinPriceLoggerTests
//
//  Created by Arpit Dixit on 02/04/22.
//

import XCTest
import Swinject
import SwinjectAutoregistration

@testable import BitcoinPriceLogger

// Autoregister can handle PriceResponse and Price as long as there are no optionals in the initializer.

extension PriceResponse {
  init(data: Price) {
    self.init(data: data, warnings: nil)
  }
}

extension Price {
  init(amount: String) {
    self.init(base: .BTC, amount: amount, currency: .USD)
  }
}

class AutoregisterTests: XCTestCase {
  
  private let container = Container()
  
  // MARK: - Boilerplate methods
  
  override func setUp() {
    super.setUp()
    container.autoregister(Price.self, argument: String.self, initializer: Price.init(amount:))
    container.autoregister(PriceResponse.self, argument: Price.self, initializer: PriceResponse.init(data:))
  }
  
  override func tearDown() {
    super.tearDown()
    container.removeAll()
  }
  
  // MARK: - Tests
  
  func testPriceResponseData() {
    let price = container ~> (Price.self, argument: "898967")
    let response = container.resolve(PriceResponse.self, argument: price)! // Use of container.resolve make you to use force unwrap instead of ~>
    XCTAssertEqual(response.data.amount, "898967")
  }
  
  func testPrice() {
    let price = container ~> (Price.self, argument: "999456")
    XCTAssertEqual(price.amount, "999456")
  }

}
