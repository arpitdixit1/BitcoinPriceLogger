//
//  Error.swift
//  BitcoinPriceLogger
//
//  Created by Arpit Dixit on 02/04/22.
//

import Foundation

struct Error: Codable {
  let id: String
  let message: String
  let url: String
}
