//
//  PriceResponse.swift
//  BitcoinPriceLogger
//
//  Created by Arpit Dixit on 02/04/22.
//

import Foundation

struct PriceResponse: Codable {
  let data: Price
  let warnings: [Error]?
}
