//
//  ViewController.swift
//  BitcoinPriceLogger
//
//  Created by Arpit Dixit on 02/04/22.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak private var primaryPriceLabel: UILabel!
    @IBOutlet weak private var partialPriceLabel: UILabel!
    
    let fetcher = BitcoinPriceFetcher(networking: HTTPNetworking())
    
    private let dollarsDisplayFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.maximumFractionDigits = 0
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        formatter.currencyGroupingSeparator = ","
        return formatter
    }()
    
    private let standardFormatter = NumberFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        requestPrice()
    }
    
    // MARK: - Actions
    @IBAction private func checkAgainTapped(sender: UIButton) {
        requestPrice()
    }
    
    // MARK: - Private methods
    private func updateLabel(price: Price) {
        guard let dollars = price.components().dollars,
              let cents = price.components().cents,
              let dollarAmount = standardFormatter.number(from: dollars) else { return }
        
        primaryPriceLabel.text = dollarsDisplayFormatter.string(from: dollarAmount)
        partialPriceLabel.text = ".\(cents)"
    }
    
    private func requestPrice()  {
        fetcher.fetch { response in
            guard let response = response else { return }
            DispatchQueue.main.async { [weak self] in
                self?.updateLabel(price: response.data)
            }
        }
    }
}

